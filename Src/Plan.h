#ifndef SRC_PLAN_H

#define SRC_PLAN_H

#include <iostream>

#include "Objet.h"

class Plan : public Objet {
 public:
    float A;
    float B;
    float C;
    float D;

    Plan(Materiau mat = Materiau(), float a = 0.0, float b = 1.0, float c = 0.0, float d = 0.0) {
        Mat = mat;
        A = a;
        B = b;
        C = c;
        D = d;
    }

    virtual bool Intersect() {
        return false;
    }

    virtual void Print() {
        std::cout << "Plan : d'équation " << A << " x + " << B << " y + " << C << " z + " << D << " = 0 ";
        std::cout << " de matériau ";
        Mat.Print();
    }
};

#endif  // SRC_PLAN_H
