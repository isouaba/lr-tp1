#ifndef SRC_SOURCE_H

#define SRC_SOURCE_H

#include <iostream>

#include "Intensite.h"
#include "Point.h"

class Source {
 public:
    Intensite Int;
    Point Position;

    Source(Intensite intensite = Intensite(), Point position = Point()) {
        Int = intensite;
        Position = position;
    }

    virtual void Print() {
        std::cout << "Source en ";
        Position.Print();
        std::cout << " d'intensité ";
        Int.Print();
    }
};

#endif  // SRC_SOURCE_H
