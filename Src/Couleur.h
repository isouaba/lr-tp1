#ifndef SRC_COULEUR_H

#define SRC_COULEUR_H

#include <iostream>

class Couleur {
 public:
    float Rouge;
    float Vert;
    float Bleu;

    Couleur(float r = 0.0, float v = 0.0, float b = 0.0) {
        Rouge = r;
        Vert = v;
        Bleu = b;
    }

    void Print() {
        std::cout << "(" << Rouge << "," << Vert << "," << Bleu << ")";
    }
};

#endif  // SRC_COULEUR_H
