#ifndef SRC_SPHERE_H

#define SRC_SPHERE_H

#include <iostream>

#include "Objet.h"
#include "Point.h"

class Sphere : public Objet {
 public:
    Point Centre;
    float Rayon;

    Sphere(Materiau mat = Materiau(), Point centre = Point(), float rayon = 1.0) {
        Mat = mat;
        Centre = centre;
        Rayon = rayon;
    }

    virtual bool Intersect() {
        return false;
    }

    virtual void Print() {
        std::cout << "Sphere : de rayon " << Rayon << ", de centre ";
        Centre.Print();
        std::cout << " de matériau ";
        Mat.Print();
    }
};

#endif  // SRC_SPHERE_H
