#ifndef SRC_OBJET_H

#define SRC_OBJET_H

#include <iostream>

#include "Materiau.h"

class Objet {
 public:
    Materiau Mat;

    Objet(Materiau mat = Materiau()) {
        Mat = mat;
    }

    virtual bool Intersect() = 0;
    virtual void Print() = 0;
};

#endif  // SRC_OBJET_H
