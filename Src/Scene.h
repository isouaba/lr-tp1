#ifndef SRC_SCENE_H

#define SRC_SCENE_H

#include <string>
#include <vector>

#include "Couleur.h"
#include "Sphere.h"
#include "Plan.h"
#include "Source.h"

class Scene {
 public:
    Scene(std::string filename);
    ~Scene();
    void Print();

 private:
    Couleur fond;
    std::vector<Objet*> objets;
    Source source;
};

#endif  // SRC_SCENE_H
